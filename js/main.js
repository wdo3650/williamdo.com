/*jslint         browser : true, continue : true,
  devel  : true, indent  : 2,    maxerr   : 50,
  newcap : true, nomen   : true, plusplus : true,
  regexp : true, sloppy  : true, vars     : false,
  white  : true
*/
/*global $, require */

require.config({
	paths: {
		jQuery:[
			"libs/jquery-1.11.2.min"],
		TweenMax: [
			"libs/TweenMax.min"],
		PointerEventsPolyfill: ["libs/pointer_events_polyfill"]
	},
	shim: {
		'jQuery': {
			exports: '$'
		},
		'PointerEventsPolyfill': {
			exports: 'PointerEventsPolyfill'
		}
	}
});

require(["jQuery", "umamie/umamie.main"],
	function($, main) {
		$(document).ready(function() {
			main.initModule();
		});
	}
);
