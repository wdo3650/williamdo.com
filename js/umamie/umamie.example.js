/*jslint         browser : true, continue : true,
  devel  : true, indent  : 2,    maxerr   : 50,
  newcap : true, nomen   : true, plusplus : true,
  regexp : true, sloppy  : true, vars     : false,
  white  : true
*/
/*global $ */

var umamie = umamie || {};

umamie.example = (function () {

	var

		queue,

		initModule;

	initModule = function(files, images) {
		queue = new createjs.LoadQueue(true);
		queue.on("complete", function() {
			console.log('complte');
		});
		queue.loadManifest(files);
	};

	return {
		initModule:initModule
	};

}());