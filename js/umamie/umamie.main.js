/*jslint         browser : true, continue : true,
  devel  : true, indent  : 2,    maxerr   : 50,
  newcap : true, nomen   : true, plusplus : true,
  regexp : true, sloppy  : true, vars     : false,
  white  : true
*/
/*global $, umamie, TweenMax, PointerEventsPolyfill */
define([
		"jQuery",
		"PointerEventsPolyfill",
		"TweenMax",
		"umamie/umamie.mobileDetail",
		"umamie/umamie.orientation",
		"umamie/umamie.browser"
		],
	function($, PointerEventsPolyfill, TweenMax, mobileDetail, orientation, browser) {

		var LIST = "list",
			DETAIL = "detail",

			screenWidth, lastScroll,
			siteState = LIST,

			$window, $projects, $redLinks,
			$mobileDetails, $wrapper, $document,
			$titles,

			initModule, cacheJQ, onResize,
			setListeners, showBackground, hideBackground,
			navigate, redLinkNavigate, openMobileView,
			onCloseMobileView, preventDefault, getSiteState;

		initModule = function() {
			PointerEventsPolyfill.initialize({});
			cacheJQ();
			$window.on("resize", onResize);
			onResize();
			setListeners();
			TweenMax.set($mobileDetails.get(0), {x:screenWidth});
			mobileDetail.initModule();
			orientation.initModule();
		};

		cacheJQ = function() {
			$window = $(window);
			$document = $(document);
			$wrapper = $('#wrapper');
			$projects = $('.project-details');
			$redLinks = $('.red-link');
			$mobileDetails = $('#mobile-details');
			$titles = $('.project').find('h4');
		};

		setListeners = function() {
			$projects.on("mouseover touchstart", showBackground);
			$projects.on("mouseout touchend", hideBackground);
			$titles.on("mouseover touchstart", showBackground);
			$titles.on("mouseout touchend", hideBackground);
			$projects.click(navigate);
			$redLinks.click(redLinkNavigate);
			$document.on("closeMobileView", onCloseMobileView);
			$titles.click(navigate);
		};

		showBackground = function(event) {
			var $background = $(this).hasClass('project-details') ? $(this).children().eq(0) : $(this).next().children().eq(0);
			if(screenWidth > 414 && $background) {
				TweenMax.to($background, 0.25, {height:30, top:-2, ease:Power4.easeOut});
			}
		};

		hideBackground = function(event) {
			var $background = $(this).hasClass('project-details') ? $(this).children().eq(0) : $(this).next().children().eq(0);
			if(screenWidth > 414) {
				TweenMax.to($background, 1, {height:0, top:28, ease:Power4.easeOut});
			}
		};

		onResize = function() {
			screenWidth = $window.width();
		};

		navigate = function(event) {
			event.preventDefault();
			if(screenWidth > 414) {
				hideBackground(event);
				var link,
					$background = $(this).children().eq(0) || $(this).next().children().eq(0),
					type = $(this).find('a').attr("data-type") || $(this).next().children().eq(0).find('a').attr('data-type');
				TweenMax.to($background, 0.25, {height:0, top:30, ease:Power2.easeInOut});
				if(browser.hasFlash() || type === "functional" || type === "offline") {
					link = $(this).find('a').attr('href') || $(this).next().find('a').attr('href');
				} else {
					link = $(this).find('a').attr('data-alt') || $(this).next().find('a').attr('data-alt');
				}
				window.open(link,"_self");
			}
		};

		redLinkNavigate = function(event) {
			event.preventDefault();
			if(screenWidth <= 415) {

				if($(this).attr("data-type") === "functional") {
					window.open($(this).attr('href'),"_self");
				} else {
					openMobileView($(this).parent().parent());
				}
			}

		};

		openMobileView = function($project) {
			siteState = DETAIL;
			mobileDetail.setProject($project);
			$mobileDetails.css({display:"block"});
			TweenMax.set($mobileDetails, {y:$(document).scrollTop()});
			lastScroll = $(document).scrollTop();
			TweenMax.to($mobileDetails.get(0), 0.45, {x:0, ease:Power4.easeInOut, onComplete:function() {
				$document.scrollTop(0);
				TweenMax.set($mobileDetails.get(0), {y:0});
				$wrapper.height($window.height());
				$wrapper.css({overflow:"hidden"});
				$('body').css({overflowY:"hidden"});
			}});
		};

		onCloseMobileView = function() {
			siteState = LIST;
			$wrapper.css({overflow:"visible", height:"auto"});
			TweenMax.set($mobileDetails.get(0), {y:lastScroll});
			$document.scrollTop(lastScroll);
			$('body').css({overflowY:"visible"});
			TweenMax.to($mobileDetails.get(0), 0.45, {x:screenWidth, delay:0.15, ease:Power4.easeInOut, onComplete:function() {
				$mobileDetails.css({display:"none"});
			}});
		};

		preventDefault = function(event){
			event.originalEvent.preventDefault();
			event.originalEvent.stopImmediatePropagation();
		};

		getSiteState = function() {
			return siteState;
		};

		return {
			initModule:initModule,
			getSiteState:getSiteState,
			LIST:LIST,
			DETAIL:DETAIL
		};

	}

);
