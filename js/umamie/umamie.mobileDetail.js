/*jslint         browser : true, continue : true,
  devel  : true, indent  : 2,    maxerr   : 50,
  newcap : true, nomen   : true, plusplus : true,
  regexp : true, sloppy  : true, vars     : false,
  white  : true
*/
/*global $, umamie, TweenMax */

define(['jQuery'], function($) {
	var defaultTitleFontSize,

		$mobileDetail, $wrapper, $window,
		$closeBtn, $detailHolder, $detailHead,
		$detailFooter, $title, $subTitle,
		$imageList, $tech, $roles, $images,
		$video,

		initModule, cacheJQ, setSize,
		setProject, setListeners, triggerClose,
		preventDefault, addMedia, showBorder,
		addVideoPoster, getPoster;

	initModule = function() {
		cacheJQ();
		defaultTitleFontSize = parseInt($title.css('font-size'), 10);
		setListeners();
	};

	setSize = function() {
		var height = $window.height();
		$mobileDetail.height(height);
		$detailHolder.height(height - 170);
	};

	setListeners = function() {
		$closeBtn.on("click touchstart", triggerClose);
		$detailHead.on("mousedown touchstart", preventDefault);
		$detailFooter.on("mousedown touchstart", preventDefault);

	};

	cacheJQ = function() {
		$mobileDetail = $('#mobile-details');
		$wrapper = $('#wrapper');
		$window = $(window);
		$closeBtn = $('.close-btn');
		$detailHolder = $('.detail-holder');
		$detailFooter = $('.detail-foot');
		$detailHead = $('.detail-head');
		$title = $mobileDetail.find('h2');
		$subTitle = $mobileDetail.find('h3');
		$imageList = $detailHolder.find('div');
		$tech = $mobileDetail.find('.tech');
		$roles = $mobileDetail.find('.roles');

	};

	setProject = function($value) {
		setSize();
		$title.css({fontSize:defaultTitleFontSize});
		$title.text($value.find('h4').text());
		$subTitle.text($value.find('.column-2').text());
		$tech.text($value.find('.column-4').text());
		$roles.text($value.find('.column-3').text());
		addMedia($value.find('.project-details').data("media"));
		if($title.text().length > 20) {
			$title.css({fontSize:18});
		}
	};

	addMedia = function(images) {
		$detailHolder.find('img').remove();
		$detailHolder.find('video').remove();
		var i = 0,
			html = "";
		for(i; i < images.length; i++) {
			if(images[i].indexOf('.mp4') > -1) {
				html += '<video controls>' +
				'<source src="' + images[i] + '" type="video/mp4"></video>';
			} else {
				html += '<img src="' + images[i] + '" />';
			}

		}
		$detailHolder.append(html);
		$images = $detailHolder.find('img');
		$video = $detailHolder.find('video');
		addVideoPoster(images);
		$images.on("load", showBorder);
	};

	addVideoPoster = function (media) {
		if($video.get(0) && getPoster(media)) {
			$video.attr('poster', getPoster(media));
		}
	};

	getPoster = function(media) {
		var i = 0;
		for(i; i < media.length; i++) {
			if(media[i].indexOf(".jpg") > -1) {
				return media[i];
			}
		}
		return null;
	};

	showBorder = function() {
		$(this).css("border", "1px solid #d6d6d6");
	};

	triggerClose = function() {
		if($video.get(0) !== undefined) {
			$video.get(0).pause();
		}
		$(document).trigger({type:"closeMobileView"});
	};

	preventDefault = function(event) {
		event.preventDefault();
	};

	return {
		initModule:initModule,
		setProject:setProject,
		showBorder:showBorder
	};


});