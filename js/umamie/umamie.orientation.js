/*jslint         browser : true, continue : true,
 devel  : true, indent  : 2,    maxerr   : 50,
 newcap : true, nomen   : true, plusplus : true,
 regexp : true, sloppy  : true, vars     : false,
 white  : true
 */
/*global $, umamie, orientation */
define(['jQuery', 'umamie/umamie.main'], function($, main) {
	var lastScroll,

		$landscapeNotification, $wrapper, $mobileDetails,
		$document,

		initModule, deviceOrientationListener, cacheJQ;

	deviceOrientationListener = function() {
		if (window.orientation === 0 || window.orientation === 180) {
			// you're in PORTRAIT mode
			$landscapeNotification.css({display:'none'});
			setTimeout(function() {
				if(umamie.main.getSiteState() === umamie.main.LIST) {
					$wrapper.css({display:'block'});
				} else if (umamie.main.getSiteState() === umamie.main.DETAIL) {
					$mobileDetails.css({display:'block'});
					$wrapper.css({display:'block'});
				}
				$document.scrollTop(lastScroll);
			}, 500);
		}
		if (window.orientation === 90 || window.orientation === -90) {
			// you're in LANDSCAPE mode
			lastScroll = $document.scrollTop();
			$landscapeNotification.css({
				display:'table',
				zIndex:50,
				opacity:0.5
			});
			$wrapper.css({zIndex:0, display:'none'});
			$mobileDetails.css({zIndex:0, display:'none'});
		}

	};

	initModule = function() {
		cacheJQ();
		if(screen.width < 737) {
			window.addEventListener("orientationchange", deviceOrientationListener, false);
			deviceOrientationListener();
		}
	};

	cacheJQ = function() {
		$landscapeNotification = $('#landscape-notification');
		$wrapper = $('#wrapper');
		$mobileDetails = $('#mobile-details');
		$document = $(document);
	};

	return {
		initModule:initModule
	}
});
