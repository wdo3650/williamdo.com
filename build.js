({
	paths: {
		jQuery: "libs/jquery-1.11.2.min",
		TweenMax: "libs/TweenMax.min",
		PointerEventsPolyfill: "libs/pointer_events_polyfill"
	},
	shim: {
		'jQuery': {
			exports: '$'
		},
		'PointerEventsPolyfill': {
			exports: 'PointerEventsPolyfill'
		}
	},

	baseUrl: "js",
	name:"main",
	out: "js/main.min.js"
})